﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

    public float reloadTime = 500;
    public GameObject fireball;
    public float speed = 30;
    private float lastFireball;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject newFireball = Instantiate(fireball, transform.position + (transform.forward*1), Quaternion.identity) as GameObject;
            newFireball.GetComponent<Rigidbody>().velocity = transform.forward * speed;
        }

    }
}
