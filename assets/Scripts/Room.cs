﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class Room : MonoBehaviour
{
    public string type;
    public Color minimapColor;

    public Corridor enteringCorridor;    // The direction of the corridor that is entering this room.
    public List<Corridor> exitingCorridors;
    
    public int leftX;
    public int rightX;
    public int topY;
    public int bottomY;

    public GameObject floorTile;                           // An array of floor tile prefabs.
    public GameObject ceilingTile;                         // An array of ceiling tile prefab.
    public GameObject wallTile;                           // An array of wall tile prefabs.
    public GameObject minimapAreaPrefab;

    public GameObject minimapArea;

    public bool cleared = false;

    public void SetupTransform(int width, int height, Corridor corridor = null)
    {
        exitingCorridors = new List<Corridor>();

        int roomWidth = width;
        int roomHeight = height;

        if (corridor == null)
        {
            bottomY = 0;
            leftX = 0;
            topY = bottomY + roomHeight-1;
            rightX = leftX + roomWidth-1;
        }
        else
        {
            enteringCorridor = corridor;
            switch (corridor.direction)
            {
                case Direction.North:
                    bottomY = corridor.topY+1;
                    topY = bottomY + roomHeight - 1;

                    leftX = Random.Range(corridor.rightX - roomWidth + 1, corridor.leftX);
                    rightX = leftX + roomWidth - 1;
                    break;
                case Direction.East:
                    leftX = corridor.rightX + 1;
                    rightX = leftX + roomWidth - 1;

                    bottomY = Random.Range(corridor.topY - roomHeight + 1, corridor.bottomY);
                    topY = bottomY + roomHeight - 1;
                    break;
                case Direction.South:
                    topY = corridor.bottomY - 1;
                    bottomY = topY - roomHeight + 1;

                    leftX = Random.Range(corridor.rightX - roomWidth + 1, corridor.leftX);
                    rightX = leftX + roomWidth - 1;
                    break;
                case Direction.West:
                    rightX = corridor.leftX - 1;
                    leftX = rightX - roomWidth + 1;

                    bottomY = Random.Range(corridor.topY - roomHeight + 1, corridor.bottomY);
                    topY = bottomY + roomHeight - 1;
                    break;
            }
        }
    }

    public void InstantiateContent()
    {
        GameObject floor = Instantiate(floorTile, new Vector3((rightX + leftX) / 2.0f, 0, (topY + bottomY) / 2.0f), Quaternion.identity) as GameObject;
        floor.transform.localScale = new Vector3(rightX - leftX +1 , 1, topY - bottomY + 1);
        floor.transform.parent = gameObject.transform;
        floor.name = "Floor";
        floor.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(floor.transform.localScale.x / 2.0f, floor.transform.localScale.z / 2.0f);

        GameObject ceiling = Instantiate(ceilingTile, new Vector3((rightX + leftX) / 2.0f, 8, (topY + bottomY) / 2.0f), Quaternion.identity) as GameObject;
        ceiling.transform.localScale = new Vector3(rightX - leftX + 1, 1, topY - bottomY + 1);
        ceiling.transform.parent = gameObject.transform;
        ceiling.name = "Ceiling";
        ceiling.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(ceiling.transform.localScale.x / 2.0f, ceiling.transform.localScale.z / 2.0f);

        minimapArea = Instantiate(minimapAreaPrefab, new Vector3((rightX + leftX) / 2.0f, 20, (topY + bottomY) / 2.0f), Quaternion.identity) as GameObject;
        minimapArea.name = "Minimap Area";
        minimapArea.transform.localScale = new Vector3(rightX - leftX + 1, 1, topY - bottomY + 1);
        minimapArea.transform.parent = gameObject.transform;
        minimapArea.GetComponent<Renderer>().material.color = minimapColor;
        minimapArea.GetComponent<Renderer>().enabled = false;


        //Instantiate walls

        //left
        List<Corridor> corridors = GetCorridors(Direction.West);
        int startY = bottomY;
        foreach(Corridor c in corridors)
        {
            if(startY <= c.bottomY -1)
                CreateWall(startY, c.bottomY-1, leftX-1, leftX-1, "Left Wall");
            startY = c.topY + 1;
        }
        CreateWall(startY, topY+1, leftX-1, leftX-1, "Left Wall");
        //right
        corridors = GetCorridors(Direction.East);
        startY = bottomY;
        foreach (Corridor c in corridors)
        {
            if (startY <= c.bottomY - 1)
                CreateWall(startY, c.bottomY - 1, rightX + 1, rightX + 1, "Right Wall");
            startY = c.topY + 1;
        }
        CreateWall(startY, topY+1, rightX + 1, rightX + 1, "Right Wall");
        //top
        corridors = GetCorridors(Direction.North);
        int startX = leftX;
        foreach (Corridor c in corridors)
        {
            if (startX <= c.leftX - 1)
                CreateWall(topY+1, topY+1, startX, c.leftX-1, "Top Wall");
            startX = c.rightX + 1;
        }
        CreateWall(topY + 1, topY + 1, startX, rightX+1, "Top Wall");
        //bottom
        corridors = GetCorridors(Direction.South);
        startX = leftX;
        foreach (Corridor c in corridors)
        {
            if (startX <= c.leftX - 1)
                CreateWall(bottomY - 1, bottomY - 1, startX, c.leftX - 1, "Bottom Wall");
            startX = c.rightX + 1;
        }
        CreateWall(bottomY - 1, bottomY - 1, startX, rightX+1, "Bottom Wall");

        InstantiateSpecificContent();
    }

    public void ShowOnMinimap()
    {
        minimapArea.GetComponent<Renderer>().enabled = true;
    }

    private void CreateWall(int startY, int endY, int startX, int endX, string name)
    {
        GameObject wall = Instantiate(wallTile, new Vector3((startX + endX) / 2.0f, 4f, (startY + endY) / 2.0f), Quaternion.identity) as GameObject;
        wall.transform.localScale = new Vector3(endX - startX + 1, 8, endY - startY + 1);
        wall.transform.parent = gameObject.transform;
        wall.name = name;
        wall.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(Mathf.Max(wall.transform.localScale.x, wall.transform.localScale.z)/2.0f, wall.transform.localScale.y/2.0f);
    }

    private List<Corridor> GetCorridors(Direction dir)
    {
        List<Corridor> wallCorridors = new List<Corridor>();
        foreach(Corridor c in exitingCorridors)
        {
            if(c.direction == dir)
            {
                wallCorridors.Add(c);
            }
        }
        if(enteringCorridor != null && enteringCorridor.direction == (Direction)(((int)dir + 2) % 4))
        {
            wallCorridors.Add(enteringCorridor);
        }
        if(dir == Direction.North || dir == Direction.South)
        {
            return wallCorridors.OrderBy(c => c.leftX).ToList();
        }
        else
        {
            return wallCorridors.OrderBy(c => c.topY).ToList();
        }
    }

    public abstract void InstantiateSpecificContent();

    public void Clear()
    {
        foreach(Corridor c in exitingCorridors)
        {
            c.OpenDoor();
            c.ShowOnMinimap();
            c.endingRoom.ShowOnMinimap();
        }
        cleared = true;
        InternalClear();
    }

    public abstract void InternalClear();

    public bool InterceptsNoRoom(List<Room> rooms)
    {
        return rooms.TrueForAll(r => !InterceptsRoom(r));
    }

    public bool InterceptsNoCorridor(List<Corridor> corridors)
    {
        return corridors.TrueForAll(c => !InterceptsCorridor(c));
    }

    public bool InterceptsCorridor(Corridor c)
    {
        Rect r1 = new Rect(leftX - 1, bottomY - 1, rightX - leftX + 1 + 2, topY - bottomY + 1 + 2);
        Rect r2 = new Rect(c.leftX - 1, c.bottomY - 1, c.rightX - c.leftX + 1 + 2, c.topY - c.bottomY + 1 + 2);
        return (r1.Overlaps(r2));
    }

    public bool InterceptsRoom(Room r)
    {
        Rect r1 = new Rect(leftX - 1, bottomY - 1, rightX - leftX + 1 + 2, topY - bottomY + 1 + 2);
        Rect r2 = new Rect(r.leftX - 1, r.bottomY - 1, r.rightX - r.leftX + 1 + 2, r.topY - r.bottomY + 1 + 2);
        return (r1.Overlaps(r2));
    }
}