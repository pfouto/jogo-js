﻿using UnityEngine;
using System.Collections;

public class Key : MonoBehaviour {

    public KeyRoom roomReference;
    public GameObject keyModel;
    public float rotateSpeed = 10;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        keyModel.transform.RotateAround(this.transform.position, Vector3.up, Time.deltaTime * rotateSpeed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Player"))
        {
            roomReference.Clear();
            other.gameObject.GetComponent<PlayerKeyScript>().hasKey = true;
            GetComponent<AudioSource>().Play();

            Destroy(this.gameObject, GetComponent<AudioSource>().clip.length);
        }
    }
}
