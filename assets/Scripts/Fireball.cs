﻿using UnityEngine;
using System.Collections;

public class Fireball : MonoBehaviour {

    public GameObject explosion;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy" || other.gameObject.tag == "Enemy Sword")
        {
            other.gameObject.GetComponentInParent<Monster>().LoseHealth();
        }
        if (!other.gameObject.name.Equals("Player"))
        {
            Instantiate(explosion, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }
}
