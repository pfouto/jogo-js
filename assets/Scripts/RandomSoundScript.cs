﻿using UnityEngine;
using System.Collections;

public class RandomSoundScript : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        Invoke("RandomSound", 10.0f);
    }

    void RandomSound()
    {
        float randomTime = Random.Range(30, 60);
        GetComponent<AudioSource>().Play();
        Invoke("RandomThing", randomTime);

    }
}
