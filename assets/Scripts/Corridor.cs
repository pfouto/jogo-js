﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    North, East, South, West,
}

public class Corridor : MonoBehaviour
{
    public int corridorLength;    // How many units long the corridor is.
    public int corridorWidth;
    public Direction direction;   // Which direction the corridor is heading from it's room.
    public GameObject door;
    public Color minimapColor = Color.gray;

    public int leftX;
    public int rightX;
    public int topY;
    public int bottomY;

    public GameObject floorTile;                           // An array of floor tile prefabs.
    public GameObject wallTile;                            // An array of wall tile prefabs.
    public GameObject doorPrefab;
    public GameObject minimapAreaPrefab;

    public GameObject minimapArea;

    public bool doorOpen = false;
    public bool openCompleted = false;
    Vector3 doorTarget;
    float speed = 3;

    public Room startingRoom;
    public Room endingRoom;

    public void SetupTransform(Room room, int lenght, int width)
    {
        startingRoom = room;
        direction = (Direction)UnityEngine.Random.Range(0, 4);
        corridorLength = lenght;
        corridorWidth = width;

        switch (direction)
        {
            case Direction.North:
                leftX = UnityEngine.Random.Range(room.leftX, room.rightX - width + 2);
                rightX = leftX + width - 1;
                bottomY = room.topY + 1;
                topY = bottomY + corridorLength - 1;
                break;
            case Direction.East:
                leftX = room.rightX + 1;
                rightX = leftX + corridorLength - 1;
                bottomY = UnityEngine.Random.Range(room.bottomY, room.topY - width + 2);
                topY = bottomY + width - 1;
                break;
            case Direction.South:
                leftX = UnityEngine.Random.Range(room.leftX, room.rightX - width + 2);
                rightX = leftX + width - 1;
                topY = room.bottomY - 1;
                bottomY = topY - corridorLength + 1;
                break;
            case Direction.West:
                rightX = room.leftX - 1;
                leftX = rightX - corridorLength + 1;
                bottomY = UnityEngine.Random.Range(room.bottomY, room.topY - width + 2);
                topY = bottomY + width - 1; 
                break;
        }
    }

    public void InstantiateContent()
    {
        GameObject floor = Instantiate(floorTile, new Vector3((rightX + leftX) / 2.0f, 0, (topY + bottomY) / 2.0f), Quaternion.identity) as GameObject;
        floor.name = "Floor";
        floor.transform.localScale = new Vector3(rightX - leftX + 1, 1, topY - bottomY + 1);
        floor.transform.parent = gameObject.transform;
        floor.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(floor.transform.localScale.x / 2.0f, floor.transform.localScale.z / 2.0f);

        GameObject ceiling = Instantiate(floorTile, new Vector3((rightX + leftX) / 2.0f, 8, (topY + bottomY) / 2.0f), Quaternion.identity) as GameObject;
        ceiling.name = "Ceiling";
        ceiling.transform.localScale = new Vector3(rightX - leftX + 1, 1, topY - bottomY + 1);
        ceiling.transform.parent = gameObject.transform;
        ceiling.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(ceiling.transform.localScale.x / 2.0f, ceiling.transform.localScale.z / 2.0f);

        Vector2 doorPos = GetDoorPosition();
        door = Instantiate(doorPrefab, new Vector3(doorPos.x, 4 , doorPos.y), Quaternion.identity) as GameObject;
        door.transform.localScale = new Vector3(direction == Direction.North || direction == Direction.South ? corridorWidth : 1, 8, direction == Direction.North || direction == Direction.South ? 1 : corridorWidth);
        door.transform.parent = gameObject.transform;
        door.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(Mathf.Max(door.transform.localScale.x, door.transform.localScale.z) / 2.0f, door.transform.localScale.y / 2.0f);

        minimapArea = Instantiate(minimapAreaPrefab, new Vector3((rightX + leftX) / 2.0f, 20, (topY + bottomY) / 2.0f), Quaternion.identity) as GameObject;
        minimapArea.name = "Minimap Area";
        minimapArea.transform.localScale = new Vector3(rightX - leftX + 1, 1, topY - bottomY + 1);
        minimapArea.transform.parent = gameObject.transform;
        minimapArea.GetComponent<Renderer>().material.color = minimapColor;
        minimapArea.GetComponent<Renderer>().enabled = false;

        GameObject wall;
        switch (direction)
        {
            case Direction.North:
            case Direction.South:
                //left
                wall = Instantiate(wallTile, new Vector3(leftX-1, 4.0f, (bottomY + topY) / 2.0f), Quaternion.identity) as GameObject;
                wall.transform.localScale = new Vector3(1, 8, topY - bottomY - 1);
                wall.transform.parent = gameObject.transform;
                wall.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(wall.transform.localScale.z / 2.0f, wall.transform.localScale.y / 2.0f);

                wall.name = "Left Wall";
                //right
                wall = Instantiate(wallTile, new Vector3(rightX+1, 4.0f, (bottomY + topY) / 2.0f), Quaternion.identity) as GameObject;
                wall.transform.localScale = new Vector3(1, 8, topY - bottomY - 1);
                wall.transform.parent = gameObject.transform;
                wall.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(wall.transform.localScale.z / 2.0f, wall.transform.localScale.y / 2.0f);
                wall.name = "Right Wall";
                break;
            case Direction.East:
            case Direction.West:
                //top
                wall = Instantiate(wallTile, new Vector3((rightX + leftX) / 2.0f, 4.0f, topY+1), Quaternion.identity) as GameObject;
                wall.transform.localScale = new Vector3(rightX - leftX - 1, 8, 1);
                wall.transform.parent = gameObject.transform;
                wall.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(wall.transform.localScale.x / 2.0f, wall.transform.localScale.y / 2.0f);

                wall.name = "Top Wall";
                //bottom
                wall = Instantiate(wallTile, new Vector3((rightX + leftX) / 2.0f, 4.0f, bottomY-1), Quaternion.identity) as GameObject;
                wall.transform.localScale = new Vector3(rightX - leftX - 1, 8, 1);
                wall.transform.parent = gameObject.transform;
                wall.transform.GetComponent<Renderer>().material.mainTextureScale = new Vector2(wall.transform.localScale.x / 2.0f, wall.transform.localScale.y / 2.0f);

                wall.name = "Bottom Wall";
                break;
        }
    }

    public void ShowOnMinimap()
    {
        minimapArea.GetComponent<Renderer>().enabled = true;
    }

    public Vector2 GetDoorPosition()
    {
        float doorX = 0;
        float doorY = 0;
        switch (direction)
        {
            case Direction.North:
                doorX = (leftX + rightX) / 2.0f;
                doorY = bottomY;
                break;
            case Direction.East:
                doorX = leftX;
                doorY = (topY+bottomY)/2.0f;
                break;
            case Direction.South:
                doorX = (leftX + rightX) / 2.0f;
                doorY = topY;
                break;
            case Direction.West:
                doorX = rightX;
                doorY = (topY + bottomY) / 2.0f;
                break;
        }
        return new Vector2(doorX, doorY);
    }

    public void OpenDoor()
    {
        doorOpen = true;
        door.GetComponent<AudioSource>().Play();
        doorTarget = door.transform.position + new Vector3(0, -8, 0);
        //door.transform.position = new Vector3(door.transform.position.x, 0, door.transform.position.z);
    }

    void Update()
    {
        if (doorOpen && !openCompleted)
        {
            float step = speed * Time.deltaTime;
            door.transform.position = Vector3.MoveTowards(door.transform.position, doorTarget, step);
            if(Vector3.Distance(door.transform.position, doorTarget) < 0.1){
                openCompleted = true;
                //door.GetComponent<AudioSource>().Stop();
            }
        }
    }

    public bool InterceptsNoRoomExcept(List<Room> rooms, Room prev)
    {
        return rooms.TrueForAll(r => (!InterceptsRoom(r) || r.Equals(prev)));
    }

    public bool InterceptsNoCorridor(List<Corridor> corridors)
    {
        return corridors.TrueForAll(c => !InterceptsCorridor(c));
    }

    public bool InterceptsCorridor(Corridor c)
    {
        Rect r1 = new Rect(leftX - 1, bottomY - 1, rightX - leftX + 1 + 2, topY - bottomY + 1 + 2);
        Rect r2 = new Rect(c.leftX - 1, c.bottomY - 1, c.rightX - c.leftX + 1 + 2, c.topY - c.bottomY + 1 + 2);
        return (r1.Overlaps(r2));
    }

    public bool InterceptsRoom(Room r)
    {
        Rect r1 = new Rect(leftX - 1, bottomY - 1, rightX - leftX + 1 + 2, topY - bottomY + 1 + 2);
        Rect r2 = new Rect(r.leftX - 1, r.bottomY - 1, r.rightX - r.leftX + 1 + 2, r.topY - r.bottomY + 1 + 2);
        return (r1.Overlaps(r2));
    }
}