﻿using UnityEngine;
using System.Collections;

public class MinimapCameraFollow : MonoBehaviour {

    private float shadowDist;

    public Light minimapLight;

    void Start()
    {
        shadowDist = QualitySettings.shadowDistance;
        minimapLight = GetComponentInChildren<Light>();
    }

    void OnPreRender()
    {
        QualitySettings.shadowDistance = 0.0f;
        //minimapLight.enabled = true;
    }
    void OnPostRender()
    {
        QualitySettings.shadowDistance = shadowDist;
        //minimapLight.enabled = false;
    }


    void LateUpdate()
    {

    }
}
