﻿using UnityEngine;
using System.Collections;

public class Chest : MonoBehaviour {

    public EndRoom roomReference;
    public GameObject chestTop;
    public float speed = 1.0f;

    public bool opening = false;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Player"))
        {
            if(other.gameObject.GetComponent<PlayerKeyScript>().hasKey == true && !opening)
            {
                opening = true;
                GetComponent<AudioSource>().Play();
            }
        }
    }

    // Update is called once per frame
    void Update () {
        if (opening)
        {
            chestTop.transform.Rotate(Vector3.back * Time.deltaTime * speed);
            if(chestTop.transform.rotation.eulerAngles.z < 270 && chestTop.transform.rotation.eulerAngles.z > 90)
            {
                opening = false;
                roomReference.Clear();
            }
        }
	}
}
