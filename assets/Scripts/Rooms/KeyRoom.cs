﻿using UnityEngine;
using System.Collections;
using System;

public class KeyRoom : Room {

    public GameObject keyPrefab;

    void Awake()
    {
        type = this.GetType().Name;
        minimapColor = Color.yellow;
    }

    public override void InstantiateSpecificContent()
    {
        GameObject key = Instantiate(keyPrefab, new Vector3((rightX + leftX) / 2.0f, 1.0f, (bottomY+topY)/2.0f), Quaternion.identity) as GameObject;
        key.GetComponent<Key>().roomReference = this;
        key.transform.parent = this.gameObject.transform;
    }

    public override void InternalClear()
    {
    }
}
