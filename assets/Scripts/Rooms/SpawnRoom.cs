﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class SpawnRoom : Room
{
    void Awake()
    {
        type = this.GetType().Name;
        minimapColor = Color.green;
    }

    public GameObject playerPrefab;

    public override void InstantiateSpecificContent()
    {
        //Instantiate player
        GameObject player = Instantiate(playerPrefab, new Vector3((rightX + leftX) / 2.0f, 3, (topY + bottomY) / 2.0f), Quaternion.identity) as GameObject;
        player.name = "Player";
        ShowOnMinimap();
    }


    public override void InternalClear()
    {
    }
}
