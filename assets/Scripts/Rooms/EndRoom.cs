﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class EndRoom : Room
{
    public GameObject chestPrefab;

    void Awake()
    {
        type = this.GetType().Name;
        minimapColor = Color.blue;
    }

    public override void InstantiateSpecificContent()
    {
        GameObject chest = Instantiate(chestPrefab, new Vector3((rightX + leftX) / 2.0f, 1.0f, (bottomY + topY) / 2.0f), Quaternion.identity) as GameObject;
        chest.transform.eulerAngles = new Vector3(chest.transform.eulerAngles.x, (90 *( ((int)enteringCorridor.direction)-1)) , chest.transform.eulerAngles.z);
        chest.GetComponent<Chest>().roomReference = this;
        chest.transform.parent = this.gameObject.transform;
    }

    public override void InternalClear()
    {
        Debug.Log("You Win");
        SceneManager.LoadScene("MainMenu");

    }
}
