﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class MonsterRoom : Room
{
    void Awake()
    {
        type = this.GetType().Name;
        minimapColor = Color.red;

    }


    public IntRange monstersRange;
    public List<Monster> monsters;
    public GameObject monsterPrefab;

    public override void InstantiateSpecificContent()
    {
        int nMonsters = monstersRange.Random;
        monsters = new List<Monster>();
        for(int i = 0; i < nMonsters; i++)
        {
            GameObject newMonster = Instantiate(monsterPrefab, new Vector3(UnityEngine.Random.Range(leftX+1, rightX),0.5f, UnityEngine.Random.Range(bottomY + 1, topY)), Quaternion.identity) as GameObject;
            newMonster.transform.parent = transform;
            Monster newMonsterScript = newMonster.GetComponent<Monster>();
            newMonsterScript.Initialize(this);
            monsters.Add(newMonsterScript);
        }
    }

    public override void InternalClear()
    {
    }

    public void OnMonsterDeath(Monster monster)
    {
        monsters.Remove(monster);
        Destroy(monster.gameObject);
        if(monsters.Count == 0)
        {
            Clear();
        }
    }
}
