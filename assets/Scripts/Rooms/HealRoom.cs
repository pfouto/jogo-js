﻿using UnityEngine;
using System.Collections;
using System;

public class HealRoom : Room
{

    public GameObject heartPrefab;

    void Awake()
    {
        type = this.GetType().Name;
        minimapColor = Color.magenta;
    }

    public override void InstantiateSpecificContent()
    {
        GameObject hearth = Instantiate(heartPrefab, new Vector3((rightX + leftX) / 2.0f, 2.0f, (bottomY + topY) / 2.0f), Quaternion.identity) as GameObject;
        hearth.GetComponent<Heart>().roomReference = this;
        hearth.transform.parent = this.gameObject.transform;
    }

    public override void InternalClear()
    {
    }
}
