﻿using UnityEngine;
using System.Collections;

public class Heart : MonoBehaviour {

    public HealRoom roomReference;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Player"))
        {
            roomReference.Clear();
            other.gameObject.GetComponent<PlayerHealth>().Heal();
            Destroy(this.gameObject);
        }
    }
}
