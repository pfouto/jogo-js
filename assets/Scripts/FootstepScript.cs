﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class FootstepScript : MonoBehaviour {

    RigidbodyFirstPersonController controllerScript;
    AudioSource audioSource;
    public AudioClip[] footsteps;

	// Use this for initialization
	void Start () {
        controllerScript = GetComponent<RigidbodyFirstPersonController>();
        audioSource = GetComponent<AudioSource>();
        StartCoroutine("Footsteps");
    }

    // Update is called once per frame
    IEnumerator Footsteps () {
        while (true)
        {
            if (controllerScript.moving)
            {
                audioSource.clip = footsteps[Random.Range(0, footsteps.Length)];
                audioSource.Play();
                Debug.Log("Foot");
                yield return new WaitForSeconds(audioSource.clip.length);
            }
            else
            {
                yield return new WaitForSeconds(0.1f);
            }
        }
	}


}
