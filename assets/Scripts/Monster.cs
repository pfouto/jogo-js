﻿using UnityEngine;
using System.Collections;

public class Monster : MonoBehaviour {

    MonsterRoom room;
    bool ready;
    bool chasing;
    bool attacking;
    int health = 2;
    Animator animator;
    GameObject player;
    float speed = 2;
    float move;
    AnimatorStateInfo stateInfo;


    int deathHash = Animator.StringToHash("Death");
    int attackHash = Animator.StringToHash("AttackTrigger");
    int walkHash = Animator.StringToHash("Walk");
    int idleHash = Animator.StringToHash("Idle");
    int hitHash = Animator.StringToHash("Hit");
	int gotHitHash = Animator.StringToHash ("Base Layer.Hit");
    int attackingHash = Animator.StringToHash("Base Layer.Attack");
    int dyingHash = Animator.StringToHash("Base Layer.Death");
	int walkingHash = Animator.StringToHash("Base Layer.Walk");

    void Start()
    {
        animator = GetComponent<Animator>();
    }
    public void Initialize(MonsterRoom room)
    {
        this.room = room;
        ready = true;
        chasing = false;
        //InvokeRepeating("LoseHealth", 0, 1);
        player = GameObject.Find("Player");
    }

    void Update () {
        stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        if (!ready)
            return;

        Vector3 targetPostition = new Vector3(player.transform.position.x,
                                       this.transform.position.y,
                                       player.transform.position.z);
        this.transform.LookAt(targetPostition);

        if (Vector3.Distance(player.transform.position, transform.position) >= 7)
        {
            chasing = false;
            //animator.SetTrigger(idleHash);
        }
        if (Vector3.Distance(player.transform.position, transform.position) < 7 && stateInfo.fullPathHash != attackingHash)
        {
            chasing = true;
        }
        if (Vector3.Distance(player.transform.position, transform.position) < 2 && stateInfo.fullPathHash != attackingHash)
        {
            attacking = true;
            chasing = false;
        }
		if (chasing && !attacking && stateInfo.fullPathHash != dyingHash && stateInfo.fullPathHash != gotHitHash && 
			stateInfo.fullPathHash != attackingHash)
        {
            move = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, move);
			Debug.Log ("moving");
            animator.SetTrigger(walkHash);
        }
        if (!chasing && attacking && stateInfo.fullPathHash != attackingHash)
        {
            //animator.SetBool("Attack", attacking);
            //LoseHealth();
            animator.SetTrigger(attackHash);
            attacking = false;
        }
        
	}

    public void LoseHealth()
    {
       
        health--;
		Debug.Log ("Got Hit");
		chasing = false;
        if (health > 0)
        {
            animator.SetTrigger(hitHash);
        }
        if (health == 0)
        {
            animator.SetTrigger(deathHash);
            //while (animator.death) { }
            Invoke("boom", 2);
            GetComponent<AudioSource>().Play();
        }
    }

    private void boom()
    {

        room.OnMonsterDeath(this);
    }
}
