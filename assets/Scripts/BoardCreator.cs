﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BoardCreator : MonoBehaviour
{
    // The type of tile that will be laid in a specific position.
    public enum TileType
    {
        Wall, Floor,
    }

    public IntRange numRooms = new IntRange(15, 20);          // The range of the number of rooms there can be.
    public IntRange roomWidth = new IntRange(3, 10);          // The range of widths rooms can have.
    public IntRange roomHeight = new IntRange(3, 10);         // The range of heights rooms can have.
    public IntRange corridorLength = new IntRange(6, 10);     // The range of lengths corridors between rooms can have.
    public int corridorWidth = 2;

    public GameObject corridorPrefab;
    public GameObject[] roomsMiddlePrefab;
    public GameObject roomEndPrefab;
    public GameObject roomSpawnPrefab;
    public GameObject roomKeyPrefab;

    public List<Room> rooms;                                     // All the rooms that are created for this board.
    public List<Corridor> corridors;                             // All the corridors that connect the rooms.
    private GameObject boardHolder;                           // GameObject that acts as a container for all other tiles.

    private void Start()
    {
        boardHolder = new GameObject("BoardHolder");

        roomWidth.m_Min = Math.Max(roomWidth.m_Min, 3);
        roomWidth.m_Max = Math.Max(roomWidth.m_Min, roomWidth.m_Max);
        roomHeight.m_Min = Math.Max(roomHeight.m_Min, 3);
        roomHeight.m_Max = Math.Max(roomHeight.m_Min, roomHeight.m_Max);

        corridorLength.m_Min = Math.Max(corridorLength.m_Min, 2);
        corridorLength.m_Max = Math.Max(corridorLength.m_Min, corridorLength.m_Max);


        CreateRoomsAndCorridors();

        InstantiateTiles();
        
        rooms[0].Clear();
    }

    void CreateRoomsAndCorridors()
    {
        int roomLimit = numRooms.Random;
        rooms = new List<Room>(roomLimit);
        corridors = new List<Corridor>();

        //First Room
        GameObject initialRoom = Instantiate(roomSpawnPrefab) as GameObject;
        initialRoom.transform.parent = boardHolder.transform;
        Room initialRoomScript = initialRoom.GetComponent<Room>();
        initialRoomScript.SetupTransform(roomWidth.Random, roomHeight.Random);
        rooms.Add(initialRoomScript);

        //Gen x rooms
        while(rooms.Count < roomLimit)
        {
            Room prev = rooms[UnityEngine.Random.Range(0, rooms.Count)];
            GameObject newCorridor = Instantiate(corridorPrefab) as GameObject;

            Corridor newCorridorScript = newCorridor.GetComponent<Corridor>();
            newCorridorScript.SetupTransform(prev, corridorLength.Random, corridorWidth);

            GameObject newRoom = null;
            if(rooms.Count == roomLimit -1)
                newRoom = Instantiate(roomEndPrefab) as GameObject;
            else
                newRoom = Instantiate(roomsMiddlePrefab[UnityEngine.Random.Range(0, roomsMiddlePrefab.Length)]) as GameObject;

            Room newRoomScript = newRoom.GetComponent<Room>();
            newRoomScript.SetupTransform(roomWidth.Random, roomHeight.Random, newCorridorScript);
            newCorridorScript.endingRoom = newRoomScript;

            if(newCorridorScript.InterceptsNoCorridor(corridors) && newCorridorScript.InterceptsNoRoomExcept(rooms, prev) && newRoomScript.InterceptsNoCorridor(corridors) && newRoomScript.InterceptsNoRoom(rooms))
            {
                prev.exitingCorridors.Add(newCorridorScript);
                corridors.Add(newCorridorScript);
                rooms.Add(newRoomScript);
                newCorridor.transform.parent = boardHolder.transform;
                newRoom.transform.parent = boardHolder.transform;
            }
            else
            {
                Destroy(newCorridor.gameObject);
                Destroy(newRoom.gameObject);
            }
        }

        //Setup key room
        int index = UnityEngine.Random.Range(1, rooms.Count - 1);//Dont want to choose first room (spawn) or last (end);
        Room chosen = rooms[index]; 
        GameObject keyRoom = Instantiate(roomKeyPrefab) as GameObject;
        Room roomKeyScrip = keyRoom.GetComponent<KeyRoom>();
        roomKeyScrip.exitingCorridors = chosen.exitingCorridors;
        foreach (Corridor c in roomKeyScrip.exitingCorridors)
        {
            c.startingRoom = roomKeyScrip;
        }
        chosen.enteringCorridor.endingRoom = roomKeyScrip;
        roomKeyScrip.enteringCorridor = chosen.enteringCorridor;
        roomKeyScrip.bottomY = chosen.bottomY;
        roomKeyScrip.topY = chosen.topY;
        roomKeyScrip.leftX = chosen.leftX;
        roomKeyScrip.rightX = chosen.rightX;
        rooms[index] = roomKeyScrip;
        keyRoom.transform.parent = boardHolder.transform;
        Destroy(chosen.gameObject);
    }

    void InstantiateTiles()
    {
        foreach (Corridor c in corridors)
        {
            c.InstantiateContent();
        }

        foreach (Room r in rooms)
        {
            r.InstantiateContent();
        }
    }
}