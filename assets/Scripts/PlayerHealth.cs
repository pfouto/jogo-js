﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

    int health = 3;

    public void TakeDamage()
    {
        health--;
        if(health <= 0)
        {
            Application.Quit();
        }
    }

    public void Heal()
    {
        health++;
        Debug.Log("New health: " + health);
    }
}
